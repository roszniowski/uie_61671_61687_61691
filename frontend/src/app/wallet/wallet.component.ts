import { Component, OnInit } from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {slideInAnimation} from "./animations";

@Component({
  selector: 'app-home',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.scss'],
  animations: [
    slideInAnimation
  ]
})
export class WalletComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  prepareRoute(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}

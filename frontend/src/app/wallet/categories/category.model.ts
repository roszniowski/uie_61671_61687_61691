import {IconProp} from "@fortawesome/fontawesome-svg-core";

export class Category {
  constructor(
    public id: number,
    public name: string,
    public icon: IconProp,
    public color: string,
    public type: number) {
  }
}

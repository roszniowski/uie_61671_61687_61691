import { Injectable } from '@angular/core';
import { Category } from "./category.model";
import { HttpClient } from '@angular/common/http';
import { API } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  category: Category;
  balance: number;
  categories: Category[];
  type: Category['type'];

  constructor(private http: HttpClient) {

  }

  getIndexData(data?) {
    return this.http.get(API.url + 'index', {
      params: {
        type: data && data['type'] ? data['type'] : '1'
      }
    });
  }

  getCategory(category: Category) {
    return this.http.get(API.url + 'transactions', {
      params: {
        category: category.id.toString()
      }
    });
  }
}

import { Injectable } from '@angular/core';
import { Category } from './categories/category.model';

@Injectable({
  providedIn: 'root'
})
export class WalletService {
  type: Category['type'];

  constructor() {
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data, Params, Router } from "@angular/router";
import { MatBottomSheet } from "@angular/material/bottom-sheet";
import { CategoriesListComponent } from "../categories/categories-list/categories-list.component";
import { CategoriesService } from "../categories/categories.service";
import { Category } from "../categories/category.model";
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Transaction } from "./transaction.model";
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import * as _moment from 'moment';
// @ts-ignore
import { default as _rollupMoment } from 'moment';
import { TransactionsService } from './transactions.service';
import { WalletService } from '../wallet.service';

const moment = _rollupMoment || _moment;

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class TransactionsComponent implements OnInit {
  category: Category;
  categorySelected: boolean = false;
  transactionsForm: FormGroup;
  type: Category['type'];
  constructor(private route: ActivatedRoute,
              private _bottomSheet: MatBottomSheet,
              private categoriesService: CategoriesService,
              private formBuilder: FormBuilder,
              public router: Router,
              private transactionsService: TransactionsService,
              private walletService: WalletService) {
    this.transactionsForm = this.formBuilder.group({
      date: moment(),
      value: new FormControl('', [Validators.required, Validators.min(0), Validators.max(2147483647)]),
      desc: ''
    })
  }

  ngOnInit() {
    this.route.params.subscribe((category: Category) => {
      if (!category['id']) return;
      this.categoriesService.getCategory(category).subscribe(
        (data) => {
          this.category = data['category'];
          this.categorySelected = true;
        }
      );
    });
  }

  onSubmit(transaction: Transaction, category: Category) {
    this.transactionsService.addTransaction(transaction, category).subscribe(
      (data) => {
          console.log(data);
      },
      (error) => {
        console.log(error);
      }
    );
    setTimeout(() => {
      this.router.navigate(['']);
    }, 400);

    console.warn('Your order has been submitted', transaction, category.id);
  }

  addToSelected() {
    this.onSubmit(this.transactionsForm.value, this.category)
  }

  showCategories(): void {
    this.route.data.subscribe(
      (data: Data) => {
        if(data && data['type']) this.type = data['type'];
      }
    );
    this._bottomSheet.open(CategoriesListComponent, {data: { type: this.type }});
    this._bottomSheet._openedBottomSheetRef.afterDismissed().subscribe(
      (category: Category) => {
        if (category == null) {
          this.categorySelected = false;
        } else {
          this.category = category;
          this.onSubmit(this.transactionsForm.value, category);
        }
      }
    );
  }

  setTwoNumberDecimal($event) {
    $event.target.value = parseFloat($event.target.value).toFixed(2);
  }

}

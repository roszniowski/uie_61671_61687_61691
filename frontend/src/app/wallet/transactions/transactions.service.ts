import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { API } from '../../../environments/environment';
import { Transaction } from './transaction.model';
import { Category } from '../categories/category.model';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {

  constructor(private http: HttpClient) {
  }

  addTransaction(transaction: Transaction, category: Category){
    return this.http.post(API.url + 'transactions/add', {
      transaction: transaction,
      category: category.id,
      type: category.type
    });
  }

  getTransactionList(){
    return this.http.get(API.url + 'transactions/list');
  }
}

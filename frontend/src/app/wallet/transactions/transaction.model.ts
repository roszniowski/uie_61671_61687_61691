export class Transaction {
  constructor(
    public date: Date,
    public value: number,
    public desc: string,
    public category: string) {}
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WalletComponent} from "./wallet/wallet.component";
import {CategoriesComponent} from "./wallet/categories/categories.component";
import {TransactionsComponent} from "./wallet/transactions/transactions.component";


const routes: Routes = [
  {path: '', component: WalletComponent, children: [
      {path: '', component: CategoriesComponent, data: {animation: 'TransactionPage'},},
      {path: 'income', component: TransactionsComponent, data: {animation: 'CategoriesPage', type: 2}},
      {path: 'income/:id', component: TransactionsComponent, data: {animation: 'CategoriesPage'}},
      {path: 'expense', component: TransactionsComponent, data: {animation: 'CategoriesPage', type: 1}},
      {path: 'expense/:id', component: TransactionsComponent, data: {animation: 'CategoriesPage'}},
    ]},
  //{path: 'transaction', component: TransactionComponent, data: {animation: 'HomePage', data: 'somehting'} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

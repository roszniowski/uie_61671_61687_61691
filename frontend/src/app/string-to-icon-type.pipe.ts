import { Pipe, PipeTransform } from '@angular/core';
import {
  faAppleAlt, faBaby, faBook, faBus,
  faCar, faCocktail, faCouch, faDesktop, faDollarSign, faFilm,
  faFutbol,
  faGamepad, faGift, faGraduationCap,
  faHome, faMoneyBill, faPhone, faPiggyBank,
  faPills, faPlane, faShoppingBasket,
  faTshirt,
  faUtensils
} from "@fortawesome/free-solid-svg-icons";

const stringIconMapper = {

  'faAppleAlt': faAppleAlt,
  'faBaby': faBaby,
  'faBook': faBook,
  'faBus': faBus,
  'faCar': faCar,
  'faCocktail': faCocktail,
  'faCouch': faCouch,
  'faDesktop': faDesktop,
  'faFilm': faFilm,
  'faFutbol': faFutbol,
  'faGamepad': faGamepad,
  'faGift': faGift,
  'faGraduationCap': faGraduationCap,
  'faHome': faHome,
  'faPhone': faPhone,
  'faPills': faPills,
  'faPlane': faPlane,
  'faShoppingBasket': faShoppingBasket,
  'faTshirt': faTshirt,
  'faUtensils': faUtensils,
  'faDollarSign': faDollarSign,
  'faMoneyBill': faMoneyBill,
  'faPiggyBank': faPiggyBank
};

@Pipe({
  name: 'stringToIconType'
})
export class StringToIconTypePipe implements PipeTransform {

  transform(string) {
    return stringIconMapper[string];
  }

}

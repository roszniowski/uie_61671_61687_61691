<?php


namespace App\Service;

use App\Entity\Category;
use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Doctrine\ORM\EntityManagerInterface;

class TransactionService
{
    private $transactionRepository;
    private $em;

    /**
     * @param TransactionRepository $transactionRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(TransactionRepository $transactionRepository, EntityManagerInterface $em)
    {
        $this->transactionRepository = $transactionRepository;
        $this->em = $em;
    }

    public function getTransactionsList()
    {
        return $this->transactionRepository->findAll();
    }

    public function getBalance()
    {
        return $this->transactionRepository->getBalance();
    }

    public function addTransaction($data)
    {
        $transactionDetails = $data['transaction'];
        $creationDate = date('D, d M Y H:i:s \G\M\T', strtotime($transactionDetails['date']));
        $transaction = new Transaction();
        $transaction->setCreatedAt(\DateTime::createFromFormat('D, d M Y H:i:s \G\M\T', $creationDate));
        $transaction->setDescription($transactionDetails['desc']);
        $data['type'] === 1 ? $transaction->setValue(-$transactionDetails['value']) : $transaction->setValue($transactionDetails['value']);
        $transaction->setCategory($this->em->getReference(Category::class, $data['category']));
        return $this->transactionRepository->add($transaction);
    }
}

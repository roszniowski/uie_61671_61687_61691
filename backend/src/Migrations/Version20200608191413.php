<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608191413 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Ubrania\', \'faTshirt\', \'pink\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Dom\', \'faHome\', \'darkseagreen\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Auto\', \'faCar\', \'indianred\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Jedzenie\', \'faUtensils\', \'dodgerblue\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Owoce\', \'faAppleAlt\', \'red\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Meble\', \'faCouch\', \'mediumpurple\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Zakupy\', \'faShoppingBasket\', \'sandybrown\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Sport\', \'faFutbol\', \'black\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Leki\', \'faPills\', \'cornflowerblue\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Elektronika\', \'faDesktop\', \'gray\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Transport lotniczy\', \'faPlane\', \'darkseagreen\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Gry\', \'faGamepad\', \'darkorange\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Imprezy\', \'faCocktail\', \'hotpink\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Transport\', \'faBus\', \'black\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Telefon\', \'faPhone\', \'rebeccapurple\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Książki\', \'faBook\', \'indianred\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Dzieci\', \'faBaby\', \'darkorange\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Edukacja\', \'faGraduationCap\', \'cornflowerblue\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Kino\', \'faFilm\', \'gray\')');
        $this->addSql('INSERT INTO category (name, icon, color) VALUES (\'Prezenty\', \'faGift\', \'mediumpurple\')');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
    }
}

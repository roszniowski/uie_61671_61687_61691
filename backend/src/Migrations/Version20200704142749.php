<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200704142749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE category ADD type INT NOT NULL');
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Ubrania', 'faTshirt', 'pink', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Dom', 'faHome', 'darkseagreen', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Auto', 'faCar', 'indianred', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Jedzenie', 'faUtensils', 'dodgerblue', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Owoce', 'faAppleAlt', 'red', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Meble', 'faCouch', 'mediumpurple', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Zakupy', 'faShoppingBasket', 'sandybrown', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Sport', 'faFutbol', 'black', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Leki', 'faPills', 'cornflowerblue', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Elektronika', 'faDesktop', 'gray', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Transport lotniczy', 'faPlane', 'darkseagreen', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Gry', 'faGamepad', 'darkorange', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Imprezy', 'faCocktail', 'hotpink', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Transport', 'faBus', 'black', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Telefon', 'faPhone', 'rebeccapurple', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Książki', 'faBook', 'indianred', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Dzieci', 'faBaby', 'darkorange', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Edukacja', 'faGraduationCap', 'cornflowerblue', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Kino', 'faFilm', 'gray', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Prezenty', 'faGift', 'mediumpurple', 1)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Pensja', 'faDollarSign', 'darkseagreen', 2)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Wpłaty', 'faMoneyBill', 'gray', 2)");
        $this->addSql("INSERT INTO category (name, icon, color, type) VALUES ('Oszczędności', 'faPiggyBank', 'hotpink', 2)");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE category DROP type');
    }
}
